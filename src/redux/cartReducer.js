import { actions } from './cartActions';
import { actions as authActions } from './authActions';
import { actions as productActions } from './productsActions';
import { actions as billingsActions } from './billingsActions';
import { suffixes } from './store';

const initialState = {
  id: null,
  products: {},
  status: null,
  user_id: null,
  loading: false,
  error: null
};

export default function cartReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_CART + suffixes.START:
      return {
        ...state,
        loading: true,
        error: null
      }
    case actions.GET_CART + suffixes.ERROR:
      return {
        ...state,
        loading: false,
        error: true
      }
    case actions.GET_CART + suffixes.SUCCESS:
      return {
        ...state,
        ...action.payload[0],
        products: Array.isArray(action.payload[0].products) ? {} : action.payload[0].products,
        loading: false
      }
    case actions.ADD_PRODUCT + suffixes.START:
      return {
        ...state,
        loading: true,
        error: null
      }
    case actions.ADD_PRODUCT + suffixes.ERROR:
      return {
        ...state,
        loading: false,
        error: true
      }
    case actions.ADD_PRODUCT + suffixes.SUCCESS:
      const newProductQuantity = action.meta.newQuantity ? action.meta.newQuantity : state.products[action.meta.product.id] ? parseInt(state.products[action.meta.product.id].quantity) + 1 : 1;
      return {
        ...state,
        loading: false,
        products: {
          ...state.products,
          [action.meta.product.id]: {
            ...action.meta.product,
            quantity: newProductQuantity,
          }
        }
      }
    case actions.REMOVE_PRODUCT + suffixes.START:
      return {
        ...state,
        products: {
          ...state.products,
          [action.meta]: {
            ...state.products[action.meta],
            loading: true
          }
        }
      }
    case actions.REMOVE_PRODUCT + suffixes.ERROR:
      return {
        ...state,
        products: {
          ...state.products,
          [action.meta]: {
            ...state.products[action.meta],
            loading: false,
            error: true
          }
        }
      }
    case productActions.SET_PRODUCT_REPUTATION + suffixes.START:
      return {
        ...state,
        products: {
          ...state.products,
          [action.meta]: {
            ...state.products[action.meta],
            settingReputation: true,
            message: null
          }
        }
      }
    case productActions.SET_PRODUCT_REPUTATION + suffixes.SUCCESS:
    case productActions.SET_PRODUCT_REPUTATION + suffixes.ERROR:
      return {
        ...state,
        products: {
          ...state.products,
          [action.meta]: {
            ...state.products[action.meta],
            settingReputation: false,
            message: action.payload.message
          }
        }
      }
    case actions.REMOVE_PRODUCT + suffixes.SUCCESS:
      const newProductsCarts = {
        ...state.products
      };
      delete newProductsCarts[action.meta];
      return {
        ...state,
        products: {
          ...newProductsCarts
        }
      }
    case billingsActions.PAY + suffixes.SUCCESS:
    case authActions.LOGOUT + suffixes.SUCCESS:
    case authActions.LOGOUT + suffixes.ERROR:
      return { ...initialState }
    default:
      return state;
  }
}