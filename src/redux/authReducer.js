import { actions } from './authActions';
import { actions as billingsActions } from './billingsActions';
import { suffixes } from './store';
import { saveSessionInLocalStorage, updateUserBalanceInLocalStorage } from '../utils/functions';
import Axios from '../utils/Axios';

const initialState = {
  loading: false,
  logingOut: false,
  user: null,
  error: null,
};

function authReducer(state = initialState, action) {
  switch (action.type) {
    case actions.LOGIN + suffixes.START:
      return {
        ...state,
        loading: true,
        error: null
      }
    case actions.LOGIN + suffixes.SUCCESS:
      saveSessionInLocalStorage(action.payload);
      return {
        ...state,
        loading: false,
        token: action.payload.token,
        user: {
          ...action.payload.user
        }
      }
    case actions.LOGIN + suffixes.ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload.message
      }
    case actions.REGISTER + suffixes.START:
      return {
        ...state,
        loading: true,
        error: null
      }
    case actions.REGISTER + suffixes.SUCCESS:
      return {
        ...state,
        loading: false,
      }
    case actions.REGISTER + suffixes.ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload.message
      }
    case actions.SET_USER_DATA:
      return {
        ...state,
        token: action.userData.token,
        user: {
          ...action.userData.user
        }
      }
    case actions.LOGOUT + suffixes.START:
      return {
        ...state,
        logingOut: true,
        error: null
      }
    case billingsActions.PAY + suffixes.SUCCESS:
      updateUserBalanceInLocalStorage(action.payload.new_user_balance);
      return {
        ...state,
        user: {
          ...state.user,
          balance: action.payload.new_user_balance
        }
      }

    case actions.LOGOUT + suffixes.SUCCESS:
    case actions.LOGOUT + suffixes.ERROR:
      // clear auth token from requests
      Axios.authToken = '';
      return { ...initialState }
    default:
      return state;
  }
}
export default authReducer;