// action types
export const actions = {
  GET_CART: 'GET_CART',
  ADD_PRODUCT: 'ADD_PRODUCT',
  REMOVE_PRODUCT: 'REMOVE_PRODUCT',
}


/**
 * GET User Cart
 *
 * @export
 * @param {*} axios
 * @returns
 */
export function getUserCart(axios) {
  return {
    type: actions.GET_CART,
    payload: axios.send()
  }
}

/**
 * Add or update a product into user cart
 *
 * @export
 * @param {*} axios
 * @returns
 */
export function addProductToCart(axios, product, newQuantity = undefined) {
  return {
    type: actions.ADD_PRODUCT,
    payload: axios.send(),
    meta: { product, newQuantity }
  }
}

/**
 * remove a product from user cart
 *
 * @export
 * @param {*} axios
 * @returns
 */
export function removeProduct(axios, productId) {
  return {
    type: actions.REMOVE_PRODUCT,
    payload: axios.send(),
    meta: productId
  }
}