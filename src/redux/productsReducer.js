import { actions } from './productsActions';
import { suffixes } from './store';
import { actions as authActions } from './authActions';

const initialState = {
  products: {},
  productsIds: [],
  loading: false,
  error: null
};

export default function productsReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_PRODUCTS + suffixes.START:
      return {
        ...state,
        loading: true,
        error: null
      }
    case actions.GET_PRODUCT_REPUTATION + suffixes.START:
      return {
        ...state,
        products: {
          ...state.products,
          [action.meta]: {
            ...state.products[action.meta],
            loading: true
          }
        }
      }
    case actions.GET_PRODUCT_REPUTATION + suffixes.SUCCESS:
    case actions.GET_PRODUCT_REPUTATION + suffixes.ERROR:
      return {
        ...state,
        products: {
          ...state.products,
          [action.meta]: {
            ...state.products[action.meta],
            reputation: action.payload.product_reputation,
            loading: false
          }
        }
      }
    case actions.GET_PRODUCTS + suffixes.ERROR:
      return {
        ...state,
        loading: false,
        error: true
      }
    case actions.GET_PRODUCTS + suffixes.SUCCESS:
      return {
        ...state,
        loading: false,
        products: {
          ...state.products,
          ...action.payload.data
        },
        productsIds: [
          ...state.productsIds,
          ...Object.keys(action.payload.data)
        ]
      }
    case authActions.LOGOUT + suffixes.SUCCESS:
    case authActions.LOGOUT + suffixes.ERROR:
      return { ...initialState }
    default:
      return state;
  }
}