// action types
export const actions = {
  SET_USER_DATA: "SET_USER_DATA",
  LOGIN: "LOGIN",
  REGISTER: 'REGISTER',
  LOGOUT: 'LOGOUT'
}

/**
 * Make Login request
 *
 * @export
 * @param {*} axios
 * @returns
 */
export function loginAttempt(axios) {
  return {
    type: actions.LOGIN,
    payload: axios
  }
}

/**
 * Make Log Out request
 *
 * @export
 * @param {*} axios
 * @returns
 */
export function logOutAttempt(axios) {
  return {
    type: actions.LOGOUT,
    payload: axios
  }
}

/**
 * Make Register request
 *
 * @export
 * @param {*} axios
 * @returns
 */
export function registerAttempt(axios) {
  return {
    type: actions.REGISTER,
    payload: axios
  }
}

export function setUserData(userData) {
  return {
    type: actions.SET_USER_DATA,
    userData
  }
}