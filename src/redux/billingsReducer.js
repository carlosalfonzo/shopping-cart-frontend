import { actions } from './billingsActions';
import { actions as authActions } from './authActions';
import { suffixes } from './store';

const initialState = {
  loading: false,
  billings: {},
  billingsIds: [],
  error: null,
  paymentMessage: null,
};

export default function billingsReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_ALL_BILLINGS + suffixes.START:
      return {
        ...state,
        loading: true,
        error: null
      }
    case actions.GET_ALL_BILLINGS + suffixes.ERROR:
      return {
        ...state,
        loading: false,
        error: true
      }
    case actions.GET_ALL_BILLINGS + suffixes.SUCCESS:
      return {
        ...state,
        loading: false,
        billings: {
          ...state.billings,
          ...action.payload.data
        },
        billingsIds: [
          ...state.billingsIds,
          ...Object.keys(action.payload.data)
        ]
      }
    case actions.PAY + suffixes.START:
      return {
        ...state,
        loading: true,
        error: null,
        paymentMessage: null
      }
    case actions.PAY + suffixes.ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload.message,
      }
    case actions.PAY + suffixes.SUCCESS:
      return {
        ...state,
        loading: false,
        paymentMessage: action.payload.message
      }
    case actions.CLEAR: {
      return {
        ...state,
        loading: false,
        error: null,
        paymentMessage: null
      }
    }
    case authActions.LOGOUT + suffixes.SUCCESS:
    case authActions.LOGOUT + suffixes.ERROR:
      return { ...initialState }
    default:
      return state;
  }
}