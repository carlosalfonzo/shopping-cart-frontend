// action types
export const SET_APP_BREAKPOINT = 'SET_APP_BREAKPOINT';

// actions creators
export const setAppBreakpoint = (size) => {
  return {
    type: SET_APP_BREAKPOINT,
    size
  }
}