import Axios from '../utils/Axios';
// action types
export const actions = {
  GET_PRODUCTS: 'GET_PRODUCTS',
  GET_PRODUCT_REPUTATION: 'GET_PRODUCT_REPUTATION',
  SET_PRODUCT_REPUTATION: 'SET_PRODUCT_REPUTATION',
}

/**
 * GET ALL PRODUCTS
 *
 * @export
 * @param {*} axios
 * @returns
 */
export function getAllProducts(axios) {
  return {
    type: actions.GET_PRODUCTS,
    payload: axios.send()
  }
}

/**
 * Get product reputation
 *
 * @export
 * @param {*} axios
 * @returns
 */
export function getReputation(productId) {
  const axios = new Axios({
    url: 'product-reputation',
    params: {
      product_id: productId
    }
  });
  return {
    type: actions.GET_PRODUCT_REPUTATION,
    payload: axios.send(),
    meta: productId
  }
}

/**
 * set product reputation
 *
 * @export
 * @param {*} axios
 * @returns
 */
export function rateProduct(axios, productId) {
  return {
    type: actions.SET_PRODUCT_REPUTATION,
    payload: axios,
    meta: productId
  }
}