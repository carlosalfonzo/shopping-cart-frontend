// action types
export const actions = {
  GET_ALL_BILLINGS: 'GET_ALL_BILLINGS',
  PAY: 'PAY',
  CLEAR: 'CLEAR',
}

/**
 * Get all User Billings
 *
 * @export
 * @param {*} axios
 * @returns
 */
export function getUserBillings(axios) {
  return {
    type: actions.GET_ALL_BILLINGS,
    payload: axios.send()
  }
}

/**
 * Get all User Billings
 *
 * @export
 * @param {*} axios
 * @returns
 */
export function pay(axios) {
  return {
    type: actions.PAY,
    payload: axios.send()
  }
}

/**
 * Clear reducer for new Payment
 *
 * @export
 */
export function clearReducer() {
  return {
    type: actions.CLEAR,
  }
}