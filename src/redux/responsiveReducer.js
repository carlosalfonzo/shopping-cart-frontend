import { SET_APP_BREAKPOINT } from './responsiveActions';

const initialState = {
  width: false
};

export default function responsiveReducer(state = initialState, action) {
  switch (action.type) {
    case SET_APP_BREAKPOINT:
      return {
        ...state,
        width: action.size
      }
    default:
      return state;
  }
}