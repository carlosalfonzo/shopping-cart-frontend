import React from 'react';
import Axios from './utils/Axios';
import {
  localAxiosRoute,
  productionAxiosRoute,
  appEnv
} from './utils/config';
// react routes imports
import SetAppRoutes from './containers/SetAppRoutesContainer';
import { BrowserRouter as ReactRouter } from 'react-router-dom';
// redux store
import { activeResponsiveSizeListeners } from './utils/windowsMatchmedias';
import { Provider as Redux } from 'react-redux';
import reduxStoreConfiguration from './redux/store';


// set Axios url
Axios.baseUrl = appEnv === 'production' ? productionAxiosRoute : localAxiosRoute;
// initializate the store creation
const store = reduxStoreConfiguration();
// export dispatch action function
export const dispatch = store.dispatch;
// activate windows matchMedia
activeResponsiveSizeListeners(dispatch);

export default function App() {
  return (
    <Redux store={store}>
      <ReactRouter>
        <SetAppRoutes />
      </ReactRouter>
    </Redux>
  );
}