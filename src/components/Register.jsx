import React, { useRef, useState } from 'react';
import './css/Register.css';
import Input from './Input';
import Button from './Button';
import Loader from './Loader';
import Axios from '../utils/Axios';
import { Link } from 'react-router-dom';


export default function Login(props) {
  const {
    error,
    registerAttempt,
    loading
  } = props;
  const inputValues = useRef({});
  const [successMessage, setSuccessMessage] = useState(undefined)

  function getInputChange(inputName, value) {
    inputValues.current = {
      ...inputValues.current,
      [inputName]: inputName === 'username' ? value.trim() : value
    }
  }

  function attempToRegister() {
    if (!loading) {
      const request = new Axios({
        url: 'signup',
        method: 'post',
        data: {
          ...inputValues.current
        }
      }).send();
      request.then(
        response => setSuccessMessage(response.message)
      )
      return registerAttempt(request);
    }
  }

  return (
    <div className='register-view-wrapper full flex-center'>
      <div className='register-form-container border-radius second-background border flex wrap'>
        <p className='full bold register-title higthlight-color'>Create a New User</p>
        <Input className='full' onChange={getInputChange} name='username' label='Username' />
        <Input className='full' onChange={getInputChange} name='password' label='Password' password />
        <Button className='register-button' onClick={attempToRegister} text='Register' />
        {
          loading && <Loader />
        }
        {
          (error || successMessage) && <p className='full higthlight-color register-message'>{error ? error : successMessage}</p>
        }
        <Link className='full register-link to-login overflow-ellipsis' to='/'>{successMessage ? 'Go to Log in' : 'You already have an account? Log in'}</Link>
      </div>
    </div>
  );
}