import React, { useState, Fragment } from 'react';
import './css/PaymentView.css';
import { shippingMethods } from '../utils/config';
import Axios from '../utils/Axios';
import Loader from './Loader';
import Button from './Button';

export default function PaymentView(props) {
  const {
    loading: loadingPayment,
    error: paymentError,
    paymentMessage,
    pay
  } = props;

  const [loading, toggleLoading] = useState(false);
  const [data, setData] = useState(undefined);
  const [shippingMethod, setShippingMethod] = useState(undefined);

  function requestCheckOutData(shippingName) {
    const request = new Axios({
      url: 'check-out',
      params: {
        shipping: shippingName
      }
    });
    request.send().then(
      response => {
        setData({ ...response });
        toggleLoading(false);
      }
    );
  }

  function makePaymentAttemp() {
    const request = new Axios({
      url: 'pay',
      method: 'post',
      data: {
        shipping: shippingMethod.name
      }
    });
    pay(request);
  }

  const selectShipping = (selected_shipping) => () => {
    if (!shippingMethod || shippingMethod.name !== selected_shipping.name) {
      setShippingMethod(selected_shipping);
      if (!loading) {
        toggleLoading(true);
        return requestCheckOutData(selected_shipping.name);
      }
    }
  }

  function renderCheckoutInfo() {
    return (
      <Fragment>
        <div className='shipping-options-container border-radius second-background border flex wrap'>
          <p className='full payment-title bold'>Shipping Method</p>
          {
            shippingMethods.map((shippingObject) => {
              return (
                <div
                  className='shipping-method-container flex-between wrap full'
                  key={`shipping-method-${shippingObject.name}`}
                  onClick={selectShipping(shippingObject)}>
                  <input
                    type='checkbox'
                    value={shippingObject.name}
                    className='shipping-checkbox'
                    checked={shippingMethod && shippingMethod.name === shippingObject.name}
                  />
                  <p className='shipping-label'>{shippingObject.name}</p>
                  <p className='shipping-price higthlight-color'>{`USD$ ${shippingObject.price}`}</p>
                </div>
              )
            })
          }
        </div>
        <div className='payment-form-container border-radius second-background border flex wrap'>
          <p className='full payment-title bold'>CheckOut</p>
          {
            loading ? <Loader /> : (
              shippingMethod ? (
                <Fragment>
                  <div className='payment-section full wrap flex'>
                    <p className='payment-label full'>Old User Balance:</p>
                    <p className='payment-value full higthlight-color bold'>{`USD$ ${data.old_user_balance}`}</p>
                  </div>
                  <div className='payment-section full wrap flex'>
                    <p className='payment-label full'>Cart SubTotal:</p>
                    <p className='payment-value full higthlight-color'>{`USD$ ${data.subtotal}`}</p>
                  </div>
                  <div className='payment-section full wrap flex'>
                    <p className='payment-label full'>Shipping Price:</p>
                    <p className='payment-value full higthlight-color'>{`USD$ ${shippingMethod.price}`}</p>
                  </div>
                  <div className='payment-section full wrap flex'>
                    <p className='payment-label full'>Cart Total:</p>
                    <p className='payment-value full higthlight-color bold'>{`USD$ ${data.cart_total}`}</p>
                  </div>
                  <div className='payment-section no-border full wrap flex'>
                    <p className='payment-label full'>New User Balance:</p>
                    <p className='payment-value full higthlight-color bold'>{`USD$ ${data.new_user_balance}`}</p>
                  </div>
                  <div className='full flex-center pay-button-container'>
                    <Button text='Pay' onClick={makePaymentAttemp} />
                  </div>
                </Fragment>
              ) : <p className='full higthlight-color no-shipping-message'>Select A Shipping Method Before CheckOut</p>
            )
          }
        </div>
      </Fragment>
    )
  }

  return (
    <div className='payment-view-wrapper full flex-center wrap'>
      {
        loadingPayment ? <Loader /> : (
          (paymentMessage !== null || paymentError !== null) ? <p className='server-payment-message full higthlight-color'>{paymentError ? paymentError : paymentMessage}</p> : renderCheckoutInfo()
        )
      }
    </div>
  )
}