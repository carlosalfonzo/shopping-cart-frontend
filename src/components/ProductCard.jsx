import React, { useState, Fragment } from 'react';
import Axios from '../utils/Axios';
import Loader from './Loader';

export default function ProductCard(props) {
  const {
    data: {
      id,
      image,
      price,
      loading,
      reputation,
      title
    },
    getReputation,
    productInCart,
    addProductToCart,
    loadingCart
  } = props;
  const [showAddToCart, toggleAddToCart] = useState(false);

  function showAddButton() {
    if (reputation === undefined && !loading) {
      getReputation(id);
    }
    toggleAddToCart(!showAddToCart);
  }

  function addOneProduct() {
    if (!loadingCart) {
      let request = undefined;
      // product is not present in cart, add a new one
      if (!productInCart) {
        request = {
          products_to_add: [
            {
              id: id,
              quantity: 1
            }
          ]
        };
      } else {
        request = {
          products_to_update_quantity: [
            {
              id: id,
              quantity: parseInt(productInCart.quantity) + 1
            }
          ]
        };
      }
      return addProductToCart(
        new Axios({
          url: 'add-or-remove-item-to-cart',
          method: 'post',
          data: request
        }), { ...props.data });
    }
  }

  return (
    <div className='product-card flex wrap second-background border' onMouseEnter={showAddButton} onMouseLeave={() => toggleAddToCart(false)}>
      <div className='full flex product-image-container alternative-background'>
        <img src={image} className='product-image full' alt={`${title}`} />
      </div>
      <p className='full product-title'>{title}</p>
      <p className='higthlight-color full product-price'>{`USD$ ${price}`}</p>
      {
        showAddToCart && (
          <Fragment>
            {
              loading ? <Loader /> : (
                <div className='full'>
                  <p className='full product-description'>{reputation ? `Reputation Avg: ${reputation}/5` : 'This Product has no reputation yet'}</p>
                </div>
              )
            }
            <div className='full add-to-cart-button higthlight-background' onClick={addOneProduct}>
              <p className='full light overflow-ellipsis'>Add To Cart</p>
            </div>
          </Fragment>
        )
      }
    </div>
  )
}
