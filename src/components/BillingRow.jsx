import React from 'react';
import { shippingMethodsByKind } from '../utils/config';

export default function BillingRow(props) {
  const {
    data: {
      total,
      shipping,
      cart_id,
      id
    }
  } = props;

  return (
    <div className='billing-row-container flex-between full second-background border wrap'>
      <div className='billing-number-container'>
        <p className='billing-label light'>Billing #</p>
        <p className='higthlight-color'>{id}</p>
      </div>
      <div className='shipping-kind-container'>
        <p className='billing-label light'>Billing Type:</p>
        <p className='higthlight-color'>{shippingMethodsByKind[shipping]}</p>
      </div>
      <div className='cart-container'>
        <p className='billing-label light'>Cart Number:</p>
        <p className='higthlight-color'>{cart_id}</p>
      </div>
      <div className='billing-total-container'>
        <p className='billing-label light'>Total:</p>
        <p className='higthlight-color overflow-ellipsis'>{`USD$ ${total}`}</p>
      </div>
    </div>
  )
}

