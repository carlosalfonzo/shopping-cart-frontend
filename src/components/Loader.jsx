import React from 'react';
import './css/Loader.css';


export default function Loader(props) {
  const {
    screen
  } = props;

  if (screen) {
    return (
      <div className='site-loader full flex-center'>
        <div className="lds-ripple">
          <div />
          <div />
        </div>
      </div>
    )
  }
  // Pure Css Loader source code https://loading.io/css/
  return (
    <div className='full flex-center'>
      <div className="lds-ripple">
        <div />
        <div />
      </div>
    </div>
  )
}