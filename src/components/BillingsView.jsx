import React, { useEffect } from 'react';
import './css/BillingsView.css'
import Axios from '../utils/Axios';
import Loader from './Loader';
import BillingRow from '../containers/BillingRowContainer';

export default function BillingsView(props) {
  const {
    getUserBillings,
    loading,
    billings,
  } = props;
  useEffect(requestUserBillings, []);

  function requestUserBillings() {
    if (!loading) {
      getUserBillings(
        new Axios({
          url: 'my-billings'
        })
      );
    }
  }

  return (
    <div className='billings-view-container full flex wrap max-width'>
      {
        loading && <Loader className='billings-loader' />
      }
      {
        (!loading && billings.length) && (
          <div className='billing-list-container full flex column max-width'>
            {
              billings.map(id => {
                return <BillingRow billingId={id} key={`billing-row-${id}`} />
              })
            }
          </div>
        )
      }
      {
        (!loading && !billings.length) && (
          <div className='empty-billings-container'>
            <p className='empty-billings-message higthlight-color full'>You have no Billings Yet, make your first payment!</p>
          </div>
        )
      }
    </div>
  );
}