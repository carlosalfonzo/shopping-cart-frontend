import React, { useEffect, Fragment } from 'react';
import './css/CartView.css';
import Loader from './Loader';
import ShoppingProductRow from '../containers/ShoppingProductRowContainer';
import Button from './Button';
import Axios from '../utils/Axios';

export default function CartView(props) {
  const {
    cart,
    loading,
    history: {
      push
    },
    products,
    clearReducer,
    getUserCart
  } = props;

  useEffect(requestCart, []);

  function requestCart() {
    if (!cart.id && !loading) {
      const request = new Axios({
        url: 'my-cart'
      });
      getUserCart(request);
    }
  }

  function calculcateCartSubtotal() {
    let cartSubtotal = 0;
    Object.keys(products).forEach(productId => {
      cartSubtotal = cartSubtotal + (parseFloat(products[productId].price) * parseInt(products[productId].quantity));
    })
    return `USD$ ${cartSubtotal}`;
  }

  function redirectToPayment() {
    clearReducer();
    push('/payment');
  }

  return (
    <div className='cart-view-container full flex wrap max-width'>
      {
        loading ? <Loader className='cart-loader' /> : (
          <div className='shopping-cart-container max-width flex wrap'>
            <p className='shopping-cart-title full bold'>My Shopping Cart</p>
            <div className='shopping-cart-product-list-container full flex wrap'>
              {
                Object.keys(products).length > 0 ? Object.keys(products).map(id => {
                  return (
                    <ShoppingProductRow
                      productId={id}
                      key={`shopping-product-${id}`}
                    />
                  )
                }) : (
                    <div className='flex-center wrap full'>
                      <p className='full higthlight-color empty-cart-message'>You have no products in your Cart, Add the first one!</p>
                      <Button onClick={() => push('/products')} text='Go To Products' />
                    </div>
                  )
              }
              {
                Object.keys(products).length > 0 && (
                  <Fragment>
                    <div className='cart-subtotal full flex-between wrap'>
                      <p className='bold'>Cart Subtotal:</p>
                      <p className='higthlight-color'>{calculcateCartSubtotal()}</p>
                    </div>
                    <div className='pay-button-container flex-center full'>
                      <Button onClick={redirectToPayment} text='Go To CheckOut' />
                    </div>
                  </Fragment>
                )
              }
            </div>
          </div>
        )
      }
    </div>
  )
}