import React, { useEffect } from 'react';
import Loader from './Loader';
import Button from './Button';
import ProductCardContainer from '../containers/ProductCardContainer';
import Axios from '../utils/Axios';
import './css/ProductsView.css'

export default function ProductsView(props) {
  const {
    loading,
    error,
    productsIds,
    getAllProducts,
  } = props;
  useEffect(requestProducts, []);

  function requestProducts() {
    if (!loading && !productsIds.length) {
      const request = new Axios({
        url: 'products'
      })
      getAllProducts(request);
    }
  }

  return (
    <div className='products-view-container full flex wrap max-width'>
      {
        loading && <Loader className='products-loader' />
      }
      {
        productsIds && (
          <div className='products-cards-container full flex-between wrap max-width'>
            {
              productsIds.map(id => {
                return <ProductCardContainer productId={id} key={`product-card-${id}`} />
              })
            }
          </div>
        )
      }
      {
        error && (
          <div className='products-message-error full flex-center wrap'>
            <p className='full higthlight-color message'>An error ocurred when trying to get the products data, please wait a second and try again</p>
            <Button onClick={requestProducts} text='Try Again' />
          </div>
        )
      }
    </div>
  )
}