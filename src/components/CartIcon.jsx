import React, { useEffect } from 'react';
import './css/CartIcon.css';
import Axios from '../utils/Axios';
import Loader from './Loader';
import { ReactComponent as Cart } from '../assets/icons/cart.svg';
import { goTo } from '../routes/SetAppRoutes';

export default function CartIcon(props) {
  const {
    products,
    getUserCart,
    id,
    loading
  } = props;
  useEffect(requestUserCart, []);

  function requestUserCart() {
    if (!loading && id === null) {
      const request = new Axios({
        url: 'my-cart'
      });
      getUserCart(request);
    }
  }
  return (
    <div className='cart-icon-container'>
      {
        loading ? <Loader /> : (
          <div className='header-icon-container' onClick={() => goTo('/my-cart')}>
            <Cart alt="shoppig-cart" className='shopping-cart icon-color' />
            <p className='products-count bold alternative-background'>{Object.keys(products).length}</p>
          </div>
        )
      }
    </div>
  )
}