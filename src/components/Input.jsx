import React, { useState } from 'react';
import './css/Input.css';

export default function Input(props) {
  const {
    onChange,
    className,
    numeric = false,
    name,
    password,
    label = ''
  } = props;

  const [inputValue, setInputValue] = useState('');

  function getInputValue({ target: { value } }) {
    if (value !== '') {
      if (numeric) {
        if (!isNaN(parseInt(value))) {
          setInputValue(parseInt(value));
        }
      }
      else {
        setInputValue(value)
      }
      return onChange(name, value);
    }
    setInputValue(value);
    return onChange(name, null);
  }

  return (
    <div className={`input-form-container ${className ? className : ''}`}>
      <p className='full overflow-ellipsis input-form-label light'>{`${label}:`}</p>
      <div className='input-container full'>
        <input
          className='input full border-radius'
          placeholder={name}
          onChange={getInputValue}
          value={inputValue}
          type={password ? 'password' : 'text'}
        />
      </div>
    </div>
  )
}