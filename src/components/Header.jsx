import React, { useState } from 'react';
import { ReactComponent as Arrow } from '../assets/icons/Arrow-down.svg';
import CartIcon from '../containers/CartIconContainer';
import { ReactComponent as Shop } from '../assets/icons/shop.svg';
import { ReactComponent as Cart } from '../assets/icons/cart.svg';
import { ReactComponent as Billings } from '../assets/icons/billings.svg';
import './css/Header.css';

const headerOptions = [
  {
    label: 'Products',
    link: '/products',
    icon: <Shop className='header-icon icon-color' />
  },
  {
    label: 'My Cart',
    link: '/my-cart',
    icon: <Cart className='header-icon icon-color' />
  },
  {
    label: 'My Billings',
    link: '/my-billings',
    icon: <Billings className='header-icon icon-color' />
  },
];

export default function Header(props) {
  const {
    history: {
      push
    },
    width
  } = props;
  const [showMenu, toggleMenu] = useState(false);

  const goToSection = (link) => () => {
    push(link);
    if (showMenu) toggleMenu(false);
  }

  return (
    <header className='full flex-center wrap second-background header-container'>
      {
        (showMenu || width > 900) && (
          <ul className='max-width flex-between wrap header-options-container'>
            {
              headerOptions.map(({ label, link, icon }, index) => {
                return <li key={`menu-item-${index}`} className='menu-item flex-center' onClick={goToSection(link)}>
                  {icon}
                  <p>{label}</p>
                </li>
              })
            }
            <CartIcon />
          </ul>
        )
      }
      {
        (width <= 900) && (
          <div className='full flex-center' onClick={() => toggleMenu(!showMenu)}>
            <Arrow className={`menu-arrow ${showMenu ? 'open' : 'closed'}`} />
          </div>
        )
      }
    </header>
  );
}