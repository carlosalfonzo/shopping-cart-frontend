import React, { useState, Fragment } from 'react';
import Loader from './Loader';
import Axios from '../utils/Axios';

export default function ShoppingProductRow(props) {
  const {
    data: {
      id,
      image,
      price,
      title,
      quantity
    },
    message,
    settingReputation,
    error,
    loading,
    deletingProduct,
    rateProduct,
    getReputation,
    addProductToCart,
    removeProduct
  } = props;

  const [productQuantity, setProductQuantity] = useState(parseInt(quantity));

  function removeQuantity() {
    if (productQuantity > 1) {
      setProductQuantity(productQuantity - 1);
    }
  }

  function addQuantity() {
    if (productQuantity < 20) {
      setProductQuantity(productQuantity + 1);
    }
  }

  function updateProductQuantity() {
    if (!loading) {
      addProductToCart(
        new Axios({
          url: 'add-or-remove-item-to-cart',
          method: 'post',
          data: {
            products_to_update_quantity: [
              {
                id: id,
                quantity: productQuantity
              }
            ]
          }
        })
        , { ...props.data }, productQuantity);
    }
  }

  function deleteProduct() {
    if (!loading) {
      removeProduct(
        new Axios({
          url: 'add-or-remove-item-to-cart',
          method: 'post',
          data: {
            products_to_remove: [
              id
            ]
          }
        })
        , id);
    }
  }

  const qualifyProduct = (value) => () => {
    const qualify = new Axios({
      url: 'send-product-reputation',
      method: 'post',
      data: {
        product_id: id,
        reputation: value
      }
    }).send();
    rateProduct(qualify, id);
    qualify.then(
      () => getReputation(id)
    );
  }

  return (
    <div className='shopping-product-row full flex second-background border'>
      <div className='product-image-container flex alternative-background'>
        <img src={image} className='product-image full' />
      </div>
      {
        (loading || deletingProduct) ? <Loader /> : (
          <div className='product-details-container flex wrap'>
            <span className='flex product-price-and-title-container wrap'>
              <p className='full higthlight-color shopping-product-title bold'>{title}</p>
              <p className='product-details-label'>Product Price:</p>
              <p className='product-details-value higthlight-color'>{`USD$ ${price}`}</p>
            </span>
            <span className='rate-product-container flex-center wrap'>
              <p className='full higthlight-color rate-product-title'>Rate Product</p>
              {
                settingReputation ? <Loader /> : (
                  message ? <p className='full'>{message}</p> : (
                    new Array(5).fill('foo').map((_, index) => {
                      return <p key={`rate-option-value-${index + 1}`} className='rate-product-option alternative-background bold' onClick={qualifyProduct(index + 1)}>{index + 1}</p>
                    })
                  )
                )
              }
            </span>
            <span className='quantity-container flex full'>
              <p className='product-details-label'>Quantity:</p>
              <div className='add quantity-button alternative-background bold' onClick={removeQuantity}>-</div>
              <p className='product-details-value higthlight-color bold'>{productQuantity}</p>
              <div className='remove quantity-button alternative-background bold' onClick={addQuantity}>+</div>
              <div className='product-actions-container flex'>
                <p className='higthlight-color' onClick={deleteProduct}>Delete</p>
                {
                  (parseInt(quantity) !== productQuantity) && (
                    <p className='higthlight-color' onClick={updateProductQuantity}>Confirm</p>
                  )
                }
              </div>
            </span>
          </div>
        )
      }
    </div>
  )
}