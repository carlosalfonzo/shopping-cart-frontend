import React, { useEffect, Fragment } from 'react';
import Header from '../containers/HeaderContainer';
import { privateRoutes } from '../routes/routes';
import { getSessionFromLocalStorage, clearSessionFromLocalStorage } from '../utils/functions';
import { ReactComponent as LogOut } from '../assets/icons/logout.svg';
import Axios from '../utils/Axios';
import Loader from './Loader';
import './css/Layout.css';
import { goTo } from '../routes/SetAppRoutes';

export default function Layout(props) {
  const {
    logedUser,
    setUserData,
    children,
    location: {
      pathname
    },
    logOutAttempt,
    loading
  } = props;
  useEffect(checkUserInLocalStorage, []);

  function checkUserInLocalStorage() {
    if (localStorage.getItem('shopping-user')) {
      const userData = getSessionFromLocalStorage();
      setUserData(userData);
      Axios.authToken = userData.token;
    }
  }

  function isAPrivateRoute() {
    let isPrivate = false;
    privateRoutes.forEach(({ path }) => {
      if (Array.isArray(path)) {
        path.forEach(route => {
          if (route === pathname) isPrivate = true;
        })
      } else {
        if (path === pathname) isPrivate = true;
      }
    })
    return isPrivate;
  }

  function logOut() {
    if (!loading) {
      const request = new Axios({
        url: 'logout'
      }).send();
      logOutAttempt(request);
      request.finally(
        () => {
          clearSessionFromLocalStorage();
          goTo('/');
        }
      );
    }
  }

  function setLayoutToRender() {
    // check if is a valid route and if is a user logged
    if (logedUser && isAPrivateRoute()) {
      return (
        <div className='full flex wrap'>
          <Header />
          {children}
          <div className='logout-container flex second-background wrap'>
            {
              loading ? <Loader /> : (
                <Fragment>
                  <div className='flex-between full username-balance-container'>
                    <div className='username-container flex'>
                      <p>User:</p>
                      <p className='higthlight-color username overflow-ellipsis'>{logedUser.username}</p>
                    </div>
                    <div className='balance-container flex'>
                      <p>Balance:</p>
                      <p className='higthlight-color username overflow-ellipsis'>{`USD$ ${logedUser.balance}`}</p>
                    </div>
                  </div>
                  <div className='full flex'>
                    <LogOut className='log-out-icon icon-color' onClick={logOut} />
                    <p className='second-color' onClick={logOut}>Log Out</p>
                  </div>
                </Fragment>
              )
            }
          </div>
        </div>
      );
    }
    return children;
  }

  return setLayoutToRender();
}