import { lazy } from 'react';

// components
const NotFound = lazy(() => import('../components/NotFound'));
const Login = lazy(() => import('../containers/LoginContainer'));
const Register = lazy(() => import('../containers/RegisterContainer'));
const ProductsView = lazy(() => import('../containers/ProductsViewContainer'));
const CartView = lazy(() => import('../containers/CartViewContainer'));
const BillingsView = lazy(() => import('../containers/BillingsViewContainer'));
const PaymentView = lazy(() => import('../containers/PaymentViewContainer'));

const notFound = [{
  key: 'not-found',
  component: NotFound
}];

const publicRoutes = [
  {
    exact: true,
    path: '/',
    key: 'Login',
    component: Login
  },
  {
    exact: true,
    path: '/register',
    key: 'register',
    component: Register
  },
];

export const privateRoutes = [
  {
    exact: true,
    path: ['/products', '/'],
    key: 'products',
    component: ProductsView
  },
  {
    exact: true,
    path: '/my-cart',
    key: 'my-cart',
    component: CartView
  },
  {
    exact: true,
    path: '/payment',
    key: 'payment',
    component: PaymentView
  },
  {
    exact: true,
    path: '/my-billings',
    key: 'my-billings',
    component: BillingsView
  },
];

export const routes = {
  public: [
    ...publicRoutes,
    ...notFound
  ],
  private: [
    ...privateRoutes,
    ...notFound
  ]
};
export default routes;