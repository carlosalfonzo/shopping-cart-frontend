import React, { Suspense } from 'react';
import { Switch, Route } from 'react-router-dom';
import Loader from '../components/Loader';
import Layout from '../containers/LayoutContainer';
import routes from './routes';

// export push new url to history
let push = null;
export const goTo = (url, state) => {
  if (push) {
    push(url, state);
  }
}

export default function SetAppRoutes(props) {
  const {
    routeKind,
    history: {
      push: historyPush
    }
  } = props;
  push = historyPush;

  return (
    <Layout>
      <Suspense fallback={<Loader screen />}>
        <Switch>
          {
            routes[routeKind].map(routeProps => <Route {...routeProps} />)
          }
        </Switch>
      </Suspense>
    </Layout>
  );
}