import PaymentView from '../components/PaymentView';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { pay } from '../redux/billingsActions';

export default withRouter(
  connect(
    state => {
      return {
        loading: state.billingsReducer.loading,
        error: state.billingsReducer.error,
        paymentMessage: state.billingsReducer.paymentMessage
      }
    },
    {
      pay
    }
  )(PaymentView)
);