import { connect } from 'react-redux';
import BillingRow from '../components/BillingRow';

export default connect(
  (state, { billingId }) => {
    return {
      data: state.billingsReducer.billings[billingId],
    }
  }
)(BillingRow);