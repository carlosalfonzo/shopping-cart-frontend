import Header from '../components/Header';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

export default withRouter(
  connect(
    state => {
      return {
        width: state.responsiveReducer.width
      }
    }
  )(Header)
);