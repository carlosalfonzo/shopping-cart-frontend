import BillingsView from '../components/BillingsView';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { getUserBillings } from '../redux/billingsActions';

export default withRouter(
  connect(
    state => {
      return {
        loading: state.billingsReducer.loading,
        billings: state.billingsReducer.billingsIds
      }
    },
    {
      getUserBillings
    }
  )(BillingsView)
);