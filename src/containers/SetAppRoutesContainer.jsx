import { connect } from 'react-redux';
import SetAppRoutes from '../routes/SetAppRoutes';
import { withRouter } from 'react-router-dom';

export default withRouter(
  connect(
    state => {
      return {
        routeKind: state.authReducer.user ? 'private' : 'public',
      }
    }
  )(SetAppRoutes)
);