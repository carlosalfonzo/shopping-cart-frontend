import CartView from '../components/CartView';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { getUserCart } from '../redux/cartActions';
import { clearReducer } from '../redux/billingsActions';

export default withRouter(
  connect(
    state => {
      return {
        cart: state.cartReducer,
        loading: state.cartReducer.loading,
        products: state.cartReducer.products
      }
    },
    {
      getUserCart,
      clearReducer
    }
  )(CartView)
);