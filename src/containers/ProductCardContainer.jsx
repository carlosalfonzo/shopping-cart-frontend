import { connect } from 'react-redux';
import ProductCard from '../components/ProductCard';
import { getReputation } from '../redux/productsActions';
import { addProductToCart } from '../redux/cartActions';

export default connect(
  (state, { productId }) => {
    return {
      data: state.productsReducer.products[productId],
      productInCart: state.cartReducer.products[productId],
      loadingCart: state.cartReducer.loading
    }
  },
  {
    getReputation,
    addProductToCart
  }
)(ProductCard);