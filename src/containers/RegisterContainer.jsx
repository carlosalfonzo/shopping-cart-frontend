import { connect } from 'react-redux';
import Register from '../components/Register';
import { withRouter } from 'react-router-dom';
import { registerAttempt } from '../redux/authActions';

export default withRouter(
  connect(
    state => {
      return {
        error: state.authReducer.error,
        loading: state.authReducer.loading,
      }
    },
    {
      registerAttempt
    }
  )(Register)
);