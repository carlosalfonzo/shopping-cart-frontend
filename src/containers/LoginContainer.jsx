import { connect } from 'react-redux';
import Login from '../components/Login';
import { withRouter } from 'react-router-dom';
import { loginAttempt } from '../redux/authActions';

export default withRouter(
  connect(
    state => {
      return {
        error: state.authReducer.error,
        loading: state.authReducer.loading,
      }
    },
    {
      loginAttempt
    }
  )(Login)
);