import { connect } from 'react-redux';
import ProductsView from '../components/ProductsView';
import { withRouter } from 'react-router-dom';
import { getAllProducts } from '../redux/productsActions';

export default withRouter(
  connect(
    state => {
      return {
        loading: state.productsReducer.loading,
        error: state.productsReducer.error,
        productsIds: state.productsReducer.productsIds
      }
    },
    {
      getAllProducts
    }
  )(ProductsView)
);