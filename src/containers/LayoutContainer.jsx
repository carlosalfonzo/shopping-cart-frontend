import { connect } from 'react-redux';
import Layout from '../components/Layout';
import { withRouter } from 'react-router-dom';
import { setUserData, logOutAttempt } from '../redux/authActions';

export default withRouter(
  connect(
    state => {
      return {
        logedUser: state.authReducer.user,
        loading: state.authReducer.logingOut
      }
    },
    {
      setUserData,
      logOutAttempt
    }
  )(Layout)
);