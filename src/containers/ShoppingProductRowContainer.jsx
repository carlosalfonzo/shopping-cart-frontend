import { connect } from 'react-redux';
import ShoppingProductRow from '../components/ShoppingProductRow';
import { addProductToCart, removeProduct } from '../redux/cartActions';
import { rateProduct, getReputation } from '../redux/productsActions';

export default connect(
  (state, { productId }) => {
    return {
      data: state.cartReducer.products[productId],
      loading: state.cartReducer.loading,
      deletingProduct: state.cartReducer.products[productId].loading,
      settingReputation: state.cartReducer.products[productId].settingReputation,
      error: state.cartReducer.products[productId].error,
      message: state.cartReducer.products[productId].message
    }
  },
  {
    addProductToCart,
    getReputation,
    rateProduct,
    removeProduct
  }
)(ShoppingProductRow);