import { connect } from 'react-redux';
import CartIcon from '../components/CartIcon';
import { getUserCart } from '../redux/cartActions';

export default connect(
  (state) => {
    return {
      products: state.cartReducer.products,
      id: state.cartReducer.id,
    }
  },
  {
    getUserCart
  }
)(CartIcon);