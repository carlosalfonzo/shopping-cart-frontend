import { setAppBreakpoint } from '../redux/responsiveActions';

// MediaQuery List
const AppBreakpoint_500 = window.matchMedia('(max-width:500px)');
const AppBreakpoint_900 = window.matchMedia('(max-width:900px)');
const AppBreakpoint_1280 = window.matchMedia('(min-width:1280px)');

/**
 * activate App breakpoint's matchmedias and dispatch redux responsive action
 * @param {*} dispatch 
 */
export const activeResponsiveSizeListeners = dispatch => {
  AppBreakpoint_500.addListener(() => breakpointListener(dispatch));
  AppBreakpoint_900.addListener(() => breakpointListener(dispatch));
  AppBreakpoint_1280.addListener(() => breakpointListener(dispatch));
  // activate listener
  breakpointListener(dispatch);
}

// trigger action for each breakpoint
const breakpointListener = (dispatch) => {
  if (AppBreakpoint_500.matches) {
    return dispatch(setAppBreakpoint(500));
  }
  if (AppBreakpoint_900.matches) {
    return dispatch(setAppBreakpoint(900));
  }
  if (AppBreakpoint_1280.matches) {
    return dispatch(setAppBreakpoint(1200));
  }
  else {
    return dispatch(setAppBreakpoint(1200));
  }
}
