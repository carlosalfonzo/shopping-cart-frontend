/**
 * Save Session in Local Storage
 *
 * @export
 */
export function saveSessionInLocalStorage(userData) {
  return localStorage.setItem('shopping-user', JSON.stringify(userData));
}

/**
 * Get user session from Local Storage
 *
 * @export
 */
export function getSessionFromLocalStorage() {
  return JSON.parse(localStorage.getItem('shopping-user'));
}

/**
 * Update the user balance before making a payment
 *
 * @export
 */
export function updateUserBalanceInLocalStorage(new_balance) {
  const stored_user = JSON.parse(localStorage.getItem('shopping-user'));
  return localStorage.setItem('shopping-user', JSON.stringify({
    ...stored_user,
    user: {
      ...stored_user.user,
      balance: new_balance
    }
  }));
}

/**
 * Delete user session from Local Storage
 *
 * @export
 */
export function clearSessionFromLocalStorage() {
  return localStorage.removeItem('shopping-user');
}