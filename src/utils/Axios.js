import axios from 'axios';
import { clearSessionFromLocalStorage } from '../utils/functions';
import { goTo } from '../routes/SetAppRoutes';
// auth Token Variable
let authToken = undefined;
// Base URL Variable
let baseUrl = '';

/**
 * Axios request Class
 * Implement app global setted Auth token, and baseURl
 *
 * @export
 * @class Axios
 */
export default class Axios {
  /**
   *
   * set Axios baseUrl
   * @static
   * @memberof Axios
   */
  static set baseUrl(url) {
    baseUrl = url;
  }

  /**
   *
   * set Axios token
   * @static
   * @memberof Axios
   */
  static set authToken(token) {
    authToken = token;
  }

  /**
   * Creates an instance of Axios.
   * @param {string} [endpoint='']
   * @param {*} [options={method: 'GET', axiosOptions}]
   * @memberof Axios
   */
  constructor(options = {}) {
    this.options = options;
    this.axios = this.createAxiosInstance();
    this.setDefaultAxiosConfigs();
  }

  /**
   * create Axios instance with model
   *
   * @memberof Axios
   */
  createAxiosInstance() {
    if (authToken && authToken !== '') {
      return axios.create({
        headers: {
          'Authorization': authToken
        }
      });
    }
    return axios.create();
  }

  /**
   * Set Axios default custom configs
   *
   * @memberof Axios
   */
  setDefaultAxiosConfigs() {
    // set axios base url
    this.axios.defaults.baseURL = baseUrl;
    // set axios Accept header
    this.axios.defaults.headers.post['Content-Type'] = 'application/json';
    // set axios response intereceptor to receive server response on errors
    this.axios.interceptors.response.use(
      response => response.data,
      errorResponse => {
        const {
          response: {
            data: {
              status
            }
          }
        } = errorResponse;
        if (status === 403) {  // handle unAuthorized, logout and redirect to login
          clearSessionFromLocalStorage();
          goTo('/');
        }
        return Promise.reject(errorResponse.response.data);
      });
  }

  /**
   * Make Axios request promise
   *
   * @returns {promise}
   * @memberof Axios
   */
  send() {
    return this.axios.request(this.options);
  }
}