export const localAxiosRoute = 'http://localhost/shopping-cart-api/api/';
export const productionAxiosRoute = 'http://api.zz.com.ve/api/';
export const appEnv = 'env';

export const shippingMethodsByKind = {
  1: 'UPS',
  2: 'Pick Up',
}

export const shippingMethods = [
  {
    name: 'UPS',
    price: 5
  },
  {
    name: 'Pick Up',
    price: 2
  },
]